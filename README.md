## Installation
1. Copy .env.example to .env and add correct database name and credentials
2. Run composer install or php composer.phar install
3. Run php artisan key:generate
4. Run php artisan migrate --seed
5. Run php artisan serve

## API docs
1. Run php artisan serve
2. Visit http://localhost:8000/docs

## Tests
To run test type: php artisan test

## License
This project is licensed under the [MIT license](https://opensource.org/licenses/MIT).
