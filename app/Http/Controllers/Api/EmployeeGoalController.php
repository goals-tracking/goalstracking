<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeGoalUpdateRequest;
use App\Models\Employee;
use Exception;
use Illuminate\Http\JsonResponse;

class EmployeeGoalController extends Controller
{
    /**
     * @param EmployeeGoalUpdateRequest $request
     * @return JsonResponse
     */
    public function update(EmployeeGoalUpdateRequest $request): JsonResponse
    {
        $employeeId     = $request->get('employee_id');
        $goalId         = $request->get('goal_id');
        $progressValue  = $request->get('progress');

        $employee = Employee::findOrFail($employeeId);

        $employeeHasGoal = $employee->goals->contains($goalId);

        try {
            if ($employeeHasGoal) {
                $employee->goals()->updateExistingPivot($goalId, ['progress' => $progressValue]);
            } else {
                return new JsonResponse('Goal progress can not be updated - given goal is not assigned to user.', 400);
            }
        } catch (Exception $exception) {
            return new JsonResponse('Goal progress can not be updated. ' . 'Error message: ' . $exception->getMessage(), 400);
        }

        return new JsonResponse('Goal progress updated successfully.', 200);
    }
}
