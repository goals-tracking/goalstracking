<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Employee extends Model
{
    use HasFactory;

    /**
     * @var string $table
     */
    protected $table = 'employees';

    /**
     * @var string[] $fillable
     */
    protected $fillable = [
        'name',
        'surname'
    ];

    /**
     * @return BelongsToMany
     */
    public function goals(): BelongsToMany
    {
        return $this->belongsToMany(Goal::class)->withPivot('progress');
    }
}
