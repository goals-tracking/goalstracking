<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_goal', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')->nullable()->constrained('employees', 'id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('progress');
            $table->foreignId('goal_id')->nullable()->constrained('goals', 'id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->unique(['employee_id', 'goal_id'], 'employee_goal_unique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_goal', function (Blueprint $table) {
            $table->dropUnique('employee_goal_unique');
        });
        Schema::dropIfExists('employee_goal');
    }
};
