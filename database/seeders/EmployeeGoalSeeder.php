<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\Goal;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmployeeGoalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = Employee::all();

        if ($employees)
        {
            foreach ($employees as $employee)
            {
                $employee->goals()->attach(Goal::get('id')->random(10), ['progress' => 0]);
            }
        }
    }
}
