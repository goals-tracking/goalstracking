<?php

use App\Http\Controllers\Api\EmployeeGoalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('employee-goal')->group(function () {
    Route::patch('/update', [EmployeeGoalController::class, 'update']);
});
