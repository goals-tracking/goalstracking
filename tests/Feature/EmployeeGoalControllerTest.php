<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\Goal;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class EmployeeGoalControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test check if goal progress is updated for given employee and assigned to it goal.
     *
     * @return void
     */
    public function testGoalProgresIsUpdated(): void
    {
        //GIVEN
        $employee   = Employee::factory()->create();
        $goal       = Goal::factory()->create();

        $employee->goals()->attach($goal->id, ['progress' => 0]);

        $this->assertEquals(1, $employee->goals->count());
        $this->assertEquals(0, $employee->goals()->first()->pivot->progress);

        $requestData = [
            'employee_id' => $employee->id,
            'goal_id' => $goal->id,
            'progress' => 55,
        ];

        //WHEN
        $response = $this->withHeaders(['Accept' => 'application/json'])->patch('/api/employee-goal/update', $requestData);

        //THEN
        $response->assertStatus(200);
        $response->assertJsonFragment(['Goal progress updated successfully.']);

        $this->assertEquals(55, $employee->goals()->first()->pivot->progress);
    }

    public function testRequiredValidation()
    {
        //WHEN
        $response = $this->withHeaders(['Accept' => 'application/json'])->patch('/api/employee-goal/update', []);

        //THEN
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'employee_id' => [
                    'The employee id field is required.',
                ],
                'goal_id' => [
                    'The goal id field is required.',
                ],
                'progress' => [
                    'The progress field is required.',
                ],
            ],
        ]);
    }

    /**
     * @param $progress
     * @param $error
     * @dataProvider progressTestDataProvider
     * @return void
     */
    public function testProgressValidation($progress, $error): void
    {
        //WHEN
        $response = $this->withHeaders(['Accept' => 'application/json'])->patch('/api/employee-goal/update', ['progress' => $progress]);

        //THEN
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'progress' => [
                    $error,
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    private function progressTestDataProvider()
    {
        return [
            'progress not number' => [
                'progress' => 'test',
                'error' => 'The progress must be an integer.'
            ],
            'progres greater than 100' => [
                'progress' => 101,
                'error' => 'The progress must not be greater than 100.'
            ],
            'progres lower than 0' => [
                'progress' => -1,
                'error' => 'The progress must be at least 0.'
            ],
        ];
    }

}
